package klassifikation;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		List<Path> paths = getPathsFromArgs(args);

		for (Path path : paths) {
			if (path == null) {
				System.err.println("Path is not specified as argument");
				return;
			}

			if (!path.toFile().exists()) {
				System.err.println("Path " + path + " does not exist");
				return;
			}

			if (notRawFile(path) || path.toFile().isDirectory()) {
				System.err.println(path + " is not a raw file");
				return;
			}
		}

		Classificator classificator = new Classificator();
		short[] signals1 = classificator.getSignals(paths.get(0));
		List<Long> energies1 = classificator.computeShortTermEnergies(signals1);
		classificator.plotShortTermEnergies(energies1);
		classificator.computeAverages(energies1, 40, 75);

		short[] signals2 = classificator.getSignals(paths.get(1));
		List<Long> energies2 = classificator.computeShortTermEnergies(signals2);
		classificator.classificate(energies2);
	}

	private static List<Path> getPathsFromArgs(String[] args) {
		List<Path> paths = new ArrayList<>();
		for (String string : args) {
			paths.add(Paths.get(string));
		}
		return paths;
	}

	private static boolean notRawFile(Path path) {
		PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:**.raw");
		return !matcher.matches(path);
	}

}
