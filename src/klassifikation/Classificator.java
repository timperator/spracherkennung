package klassifikation;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import com.panayotis.gnuplot.JavaPlot;
import com.panayotis.gnuplot.dataset.DataSet;
import com.panayotis.gnuplot.plot.DataSetPlot;
import com.panayotis.gnuplot.style.PlotStyle;
import com.panayotis.gnuplot.style.Style;

import merkmalsextraktion.datasets.EnergyDataSet;

public class Classificator {

	private static final int WINDOW_SIZE = 160;

	double averageSilence;
	double averageNoise;
	double varianceSilence;
	double varianceNoise;

	public short[] getSignals(Path path) {

		byte[] data;
		try {
			data = Files.readAllBytes(path);

			short[] values = new short[data.length / 2];

			int j = 0;
			for (int i = 0; i < data.length; i = i + 2) {
				values[j++] = ByteBuffer.wrap(data, i, 2).order(ByteOrder.LITTLE_ENDIAN).getShort();
			}
			return values;

		} catch (IOException e) {
			System.err.println("Unable to read file");
			System.exit(0);
			return null;
		}
	}

	public List<Long> computeShortTermEnergies(short[] signals) {

		List<Long> energies = new ArrayList<>();

		// iterate over analysis windows
		for (int i = 0; i < signals.length; i += WINDOW_SIZE) {
			long shortTermEnergy = 0;
			for (int j = i; j < i + WINDOW_SIZE; j++) {
				if (j >= signals.length) {
					break;
				}
				shortTermEnergy += signals[j] * signals[j];
			}
			energies.add(shortTermEnergy);
		}

		// energies.forEach((Long e) -> System.out.printf("%12d\n", e));

		return energies;
	}

	public void computeAverages(List<Long> energies, int startNoise, int endNoise) {

		List<Long> silence = new ArrayList<>();
		List<Long> noise = new ArrayList<>();

		for (int i = 0; i < startNoise; i++) {
			silence.add(energies.get(i));
		}

		for (int i = startNoise; i < endNoise; i++) {
			noise.add(energies.get(i));
		}

		for (int i = endNoise; i < energies.size(); i++) {
			silence.add(energies.get(i));
		}

		averageSilence = silence.stream().mapToDouble(val -> val).average().getAsDouble();
		averageNoise = noise.stream().mapToDouble(val -> val).average().getAsDouble();

		System.out.printf("Average silence:  %20.0f%n", averageSilence);
		System.out.printf("Average noise:    %20.0f%n", averageNoise);

		varianceSilence = getVariance(silence, averageSilence);
		varianceNoise = getVariance(noise, averageNoise);

		System.out.printf("Variance silence: %20.0f%n", varianceSilence);
		System.out.printf("Variance noise:   %20.0f", varianceNoise);
	}

	private double getVariance(List<Long> values, double average) {
		double sum = 0.0;
		for (Long value : values) {
			sum += (value - average) * (value - average);
		}
		return sum / values.size();
	}

	public void plotShortTermEnergies(List<Long> energies) {
		DataSet dataset = new EnergyDataSet(energies);

		DataSetPlot dataSetPlot = new DataSetPlot(dataset);
		dataSetPlot.setPlotStyle(new PlotStyle(Style.LINES));
		dataSetPlot.setTitle("Short term energy");

		createPlotter(dataSetPlot).plot();
	}

	private JavaPlot createPlotter(DataSetPlot dataset) {
		JavaPlot plotter = new JavaPlot();
		plotter.addPlot(dataset);
		return plotter;
	}

	public void classificate(List<Long> energies) {
		// TODO: implement method
	}

}
