package merkmalsextraktion;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import com.panayotis.gnuplot.JavaPlot;
import com.panayotis.gnuplot.dataset.DataSet;
import com.panayotis.gnuplot.plot.DataSetPlot;
import com.panayotis.gnuplot.style.PlotStyle;
import com.panayotis.gnuplot.style.Style;

import merkmalsextraktion.datasets.EnergyDataSet;
import merkmalsextraktion.datasets.FrequencyDataSet;

public class RawFileAnalyzer {

	private static final int ENERGY_WINDOW_SIZE = 160;
	private static final int FREQUENCY_WINDOW_SIZE = 256;

	private Path rawFile;
	private short[] voiceSignals;

	private RawFileAnalyzer(Path path) {
		this.rawFile = path;

		byte[] data;
		try {
			data = Files.readAllBytes(path);
			voiceSignals = getValuesFromData(data);

		} catch (IOException e) {
			System.err.println("Unable to read file");
			System.exit(0);
		}
	}

	public static RawFileAnalyzer fromPath(Path path) {
		return new RawFileAnalyzer(path);
	}

	public void computeShortTermEnergies(boolean withOutput) {
		List<Long> energies = getShortTermEnergies();
		if (withOutput) {
			System.out.println(String.format("Short term energy (%s)", rawFile.getFileName()));
			energies.forEach((Long e) -> System.out.println(String.format("%12d", e)));
		}
		plotShortTermEnergies(energies);
	}

	private List<Long> getShortTermEnergies() {

		List<Long> energies = new ArrayList<>();

		// iterate over analysis windows
		for (int i = 0; i < voiceSignals.length; i += ENERGY_WINDOW_SIZE) {
			long shortTermEnergy = 0;
			for (int j = i; j < i + ENERGY_WINDOW_SIZE; j++) {
				if (j >= voiceSignals.length) {
					break;
				}
				shortTermEnergy += Math.pow(voiceSignals[j], 2);
			}
			energies.add(shortTermEnergy);
		}
		return energies;
	}

	private short[] getValuesFromData(byte[] data) {

		short[] values = new short[data.length / 2];

		int j = 0;
		for (int i = 0; i < data.length; i = i + 2) {
			values[j++] = ByteBuffer.wrap(data, i, 2).order(ByteOrder.LITTLE_ENDIAN).getShort();
		}
		return values;
	}

	private void plotShortTermEnergies(List<Long> energies) {
		DataSet dataset = new EnergyDataSet(energies);

		DataSetPlot dataSetPlot = new DataSetPlot(dataset);
		dataSetPlot.setPlotStyle(new PlotStyle(Style.LINES));
		dataSetPlot.setTitle("Short term energy");

		createPlotter(dataSetPlot).plot();
	}

	private JavaPlot createPlotter(DataSetPlot dataset) {
		JavaPlot plotter = new JavaPlot();
		plotter.setTitle(rawFile.getFileName().toString());
		plotter.addPlot(dataset);
		return plotter;
	}

	public void computeShortTermFrequencies(boolean withOutput) {

		int count = 0;

		int max = voiceSignals.length / FREQUENCY_WINDOW_SIZE;
		JavaPlot plotter = new JavaPlot();

		for (int i = 0; i < voiceSignals.length; i += FREQUENCY_WINDOW_SIZE) {
			if (i + FREQUENCY_WINDOW_SIZE > voiceSignals.length) {
				break;
			}
			System.out.println("calculating frequency " + ++count + "/" + max);
			double[] frequencies = getWindowFrequencies(i, FREQUENCY_WINDOW_SIZE);
			// plotShortTermFrequencies(frequencies, count, max);

			DataSet dataset = new FrequencyDataSet(frequencies);
			DataSetPlot dataSetPlot = new DataSetPlot(dataset);
			dataSetPlot.setPlotStyle(new PlotStyle(Style.LINES));
			plotter.addPlot(dataSetPlot);
		}

		plotter.plot();
		// plotShortTermFrequencies(getWindowFrequencies(0, voiceSignals.length), 1, 1);

		if (withOutput) {
			System.out.println(count + " frequencys calculated");
		}
	}

	public double[] getWindowFrequencies(int start, int length) {

		double[] real = new double[length];
		double[] imag = new double[length];

		// initialize input signal array
		for (int i = 0; i < length; i++) {
			real[i] = voiceSignals[start + i];
		}

		int direction = 1;
		int exponent = (int) (Math.log(length) / Math.log(2));

		FFT.transform(direction, exponent, real, imag);

		double[] retval = new double[real.length];
		for (int i = 0; i < real.length; i++) {
			retval[i] = real[i] * real[i] + imag[i] + imag[i];
		}
		return retval;

	}

	private void plotShortTermFrequencies(double[] frequencies, int count, int max) {
		DataSet dataset = new FrequencyDataSet(frequencies);
		DataSetPlot dataSetPlot = new DataSetPlot(dataset);
		dataSetPlot.setPlotStyle(new PlotStyle(Style.LINES));
		dataSetPlot.setTitle(String.format("Short term frequencies (%d/%d)", count, max));
		createPlotter(dataSetPlot).plot();
	}

}
