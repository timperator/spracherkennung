package merkmalsextraktion;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;

public class Main {

	public static void main(String[] args) {

		Path path = getPathFromArgs(args);

		if (path == null) {
			System.err.println("Path is not specified as argument");
			return;
		}

		if (!path.toFile().exists()) {
			System.err.println("Path " + path + " does not exist");
			return;
		}

		if (notRawFile(path) || path.toFile().isDirectory()) {
			System.err.println(path + " is not a raw file");
			return;
		}

		RawFileAnalyzer analyzer = RawFileAnalyzer.fromPath(path);
		analyzer.computeShortTermEnergies(true);
		analyzer.computeShortTermFrequencies(true);
	}

	private static Path getPathFromArgs(String[] args) {
		if (args.length < 1) {
			return null;
		} else {
			return Paths.get(args[0]);
		}
	}

	private static boolean notRawFile(Path path) {
		PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:**.raw");
		return !matcher.matches(path);
	}

}
