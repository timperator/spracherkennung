package merkmalsextraktion.datasets;

import java.util.List;

import com.panayotis.gnuplot.dataset.DataSet;

public class EnergyDataSet implements DataSet {

	private List<Long> energies;

	public EnergyDataSet(List<Long> energies) {
		this.energies = energies;
	}

	@Override
	public int size() {
		return energies.size();
	}

	@Override
	public String getPointValue(int arg0, int arg1) {
		return String.valueOf(energies.get(arg0));
	}

	@Override
	public int getDimensions() {
		return 1;
	}
}
