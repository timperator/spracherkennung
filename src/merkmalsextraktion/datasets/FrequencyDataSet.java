package merkmalsextraktion.datasets;

import com.panayotis.gnuplot.dataset.DataSet;

public class FrequencyDataSet implements DataSet {

	private double[] frequencies;

	public FrequencyDataSet(double[] frequencies) {
		this.frequencies = frequencies;
	}

	@Override
	public int size() {
		return frequencies.length;
	}

	@Override
	public String getPointValue(int arg0, int arg1) {
		return String.valueOf(frequencies[arg0]);
	}

	@Override
	public int getDimensions() {
		return 1;
	}
}
