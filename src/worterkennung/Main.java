package worterkennung;

public class Main {

	public static void main(String[] args) {

		String target = "wette";
		String source = "brett";

		int distance = LevenshteinDistance.computeLevenshteinDistance(target, source);

		System.out.printf("%s, %s: %d", target, source, distance);

	}

}
