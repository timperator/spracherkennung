package worterkennung;

public class LevenshteinDistance {

	public static int computeLevenshteinDistance(String target, String source) {

		int sourceLength = source.length();
		int targetLength = target.length();
		int[][] matrix = new int[sourceLength + 1][targetLength + 1];

		// y-axis
		for (int i = 0; i <= sourceLength; i++) {
			matrix[i][0] = i;
		}

		// x-axis
		for (int j = 1; j <= targetLength; j++) {
			matrix[0][j] = j;
		}

		for (int i = 1; i <= sourceLength; i++) {
			for (int j = 1; j <= targetLength; j++) {
				int insert = matrix[i - 1][j] + 1;
				int delete = matrix[i][j - 1] + 1;
				int subsituteCost = ((source.charAt(i - 1) == target.charAt(j - 1)) ? 0 : 1);
				int substitute = matrix[i - 1][j - 1] + subsituteCost;
				matrix[i][j] = min(insert, delete, substitute);
			}
		}

		printMatrix(source, target, matrix);

		return matrix[sourceLength][targetLength];
	}

	private static int min(int a, int b, int c) {
		return Math.min(Math.min(a, b), c);
	}

	private static void printMatrix(String vertikal, String horizontal, int[][] matrix) {

		int horizontalLength = matrix[0].length;
		int vertikalLength = matrix.length;

		System.out.print("      ");
		for (int i = 0; i < horizontal.length(); i++) {
			System.out.print((char) (horizontal.charAt(i) & 0x5f) + "  ");
		}
		System.out.println();
		System.out.print(" ");
		for (int i = 0; i < horizontalLength; i++) {
			System.out.print("---");
		}
		System.out.println();

		for (int i = 0; i < vertikalLength; i++) {

			System.out.print(i > 0 ? ((char) (vertikal.charAt(i - 1) & 0x5f) + "|") : " |");

			for (int j = 0; j < horizontalLength; j++) {
				System.out.printf("%2d ", matrix[i][j]);
			}
			System.out.println();
		}
		System.out.println("");
	}

}
